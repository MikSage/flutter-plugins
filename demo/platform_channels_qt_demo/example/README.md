# Platform Channels + Qt demo example

Demonstrates how to use the platform_channels_qt_demo plugin.

## Usage

Get dependency

```shell
flutter-aurora pub get
```

Build aurora application

```shell
flutter-aurora build aurora --release
```
