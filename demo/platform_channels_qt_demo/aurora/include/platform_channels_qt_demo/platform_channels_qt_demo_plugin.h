/**
 * SPDX-FileCopyrightText: Copyright 2024 Open Mobile Platform LLC <community@omp.ru>
 * SPDX-License-Identifier: BSD-3-Clause
 */
#ifndef FLUTTER_PLUGIN_PLATFORM_CHANNELS_QT_DEMO_PLUGIN_H
#define FLUTTER_PLUGIN_PLATFORM_CHANNELS_QT_DEMO_PLUGIN_H

#include <platform_channels_qt_demo/globals.h>

// Add dependency Qt
#include <QNetworkConfigurationManager>

#include <flutter/plugin_registrar.h>
#include <flutter/encodable_value.h>
#include <flutter/standard_method_codec.h>
#include <flutter/event_channel.h>
#include <flutter/event_stream_handler_functions.h>

// Flutter encodable
typedef flutter::EncodableValue EncodableValue;
typedef flutter::EncodableMap EncodableMap;
typedef flutter::EncodableList EncodableList;
// Flutter events
typedef flutter::EventChannel<EncodableValue> EventChannel;
typedef flutter::EventSink<EncodableValue> EventSink;

// Enable QObject
class PLUGIN_EXPORT PlatformChannelsQtDemoPlugin final
    : public QObject
    , public flutter::Plugin
{
    Q_OBJECT
public:
    static void RegisterWithRegistrar(flutter::PluginRegistrar* registrar);

// Create slot
public slots:
    void onEventChannelSend();

private:
    // Creates a plugin that communicates on the given channel.
    PlatformChannelsQtDemoPlugin(
        std::unique_ptr<EventChannel> eventChannel
    );

    // Methods register handlers channels
    void RegisterStreamHandler();

    // Other methods
    void onEventChannelEnable();
    void onEventChannelDisable();

    // Variables for Flutter
    std::unique_ptr<EventChannel> m_eventChannel;
    std::unique_ptr<EventSink> m_sink;

    // Variables for Qt
    bool m_state;
    QNetworkConfigurationManager m_manager;
    QMetaObject::Connection m_onlineStateChangedConnection;
};

#endif /* FLUTTER_PLUGIN_PLATFORM_CHANNELS_QT_DEMO_PLUGIN_H */
