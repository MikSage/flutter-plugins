/**
 * SPDX-FileCopyrightText: Copyright 2024 Open Mobile Platform LLC <community@omp.ru>
 * SPDX-License-Identifier: BSD-3-Clause
 */
#include <platform_channels_qt_demo/platform_channels_qt_demo_plugin.h>

namespace Channels {
constexpr auto Event = "platform_channels_qt_demo";
} // namespace Channels

void PlatformChannelsQtDemoPlugin::RegisterWithRegistrar(
    flutter::PluginRegistrar *registrar) {
  // Create EventChannel with StandardMethodCodec
  auto eventChannel = std::make_unique<EventChannel>(
      registrar->messenger(), Channels::Event,
      &flutter::StandardMethodCodec::GetInstance());

  // Create plugin
  std::unique_ptr<PlatformChannelsQtDemoPlugin> plugin(
      new PlatformChannelsQtDemoPlugin(std::move(eventChannel)));

  // Register plugin
  registrar->AddPlugin(std::move(plugin));
}

PlatformChannelsQtDemoPlugin::PlatformChannelsQtDemoPlugin(
    std::unique_ptr<EventChannel> eventChannel)
    : m_eventChannel(std::move(eventChannel)) {
  // Create StreamHandler
  RegisterStreamHandler();
}

void PlatformChannelsQtDemoPlugin::RegisterStreamHandler()
{
    // Create handler for events Platform Channels
    auto handler = std::make_unique<flutter::StreamHandlerFunctions<EncodableValue>>(
        [&](const EncodableValue*,
            std::unique_ptr<flutter::EventSink<EncodableValue>>&& events
        ) -> std::unique_ptr<flutter::StreamHandlerError<EncodableValue>> {
            m_sink = std::move(events);
            onEventChannelEnable();
            return nullptr;
        },
        [&](const EncodableValue*) -> std::unique_ptr<flutter::StreamHandlerError<EncodableValue>> {
            onEventChannelDisable();
            return nullptr;
        }
    );

    // Register event
    m_eventChannel->SetStreamHandler(std::move(handler));
}

void PlatformChannelsQtDemoPlugin::onEventChannelEnable()
{
    // Send after start
    onEventChannelSend();
    // Connect listen connection
    m_onlineStateChangedConnection =
        QObject::connect(&m_manager,
                         &QNetworkConfigurationManager::onlineStateChanged,
                         this,
                         &PlatformChannelsQtDemoPlugin::onEventChannelSend);
}

void PlatformChannelsQtDemoPlugin::onEventChannelDisable()
{
    // Disconnect listen connection
    QObject::disconnect(m_onlineStateChangedConnection);
}

void PlatformChannelsQtDemoPlugin::onEventChannelSend()
{
    // Send state is change
    auto state = m_manager.isOnline();
    if (state != m_state) {
        m_state = state;
        m_sink->Success(m_manager.isOnline());
    }
}

// Add moc file
#include "moc_platform_channels_qt_demo_plugin.cpp"
