// SPDX-FileCopyrightText: Copyright 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import 'platform_channels_qt_demo_platform_interface.dart';

class PlatformChannelsQtDemo {
  Stream<bool?> stateNetworkConnect() {
    return PlatformChannelsQtDemoPlatform.instance.stateNetworkConnect();
  }
}
