# Client wrapper demo example

Demonstrates how to use the client_wrapper_demo plugin.

## Usage

Get dependency

```shell
flutter-aurora pub get
```

Build aurora application

```shell
flutter-aurora build aurora --release
```
