// SPDX-FileCopyrightText: Copyright 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
library dbus_demo;

import 'dart:ffi';
import 'package:ffi/ffi.dart';
import 'ffi_demo_bindings_generated.dart';

class FFIDemo {

  /// Bindings to native functions in `libffi_demo.so`.
  final FFIDemoBindings _bindings = FFIDemoBindings(
    DynamicLibrary.open('libffi_demo.so'),
  );

  /// Implementation of a method to obtain the device name.
  Future<String?> getDeviceName() async {
    // Method Execution
    final deviceName = _bindings.getDeviceName().cast<Utf8>().toDartString();
    // Returning the result
    return deviceName == '' ? null : deviceName;
  }
}
