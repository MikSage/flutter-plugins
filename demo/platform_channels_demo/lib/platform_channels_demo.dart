// SPDX-FileCopyrightText: Copyright 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import 'platform_channels_demo_platform_interface.dart';

class PlatformChannelsDemo {
  Future<String?> getApplicationName({String? prefix}) {
    return PlatformChannelsDemoPlatform.instance.getApplicationName(prefix);
  }
}
