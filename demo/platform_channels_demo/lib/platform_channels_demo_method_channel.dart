// SPDX-FileCopyrightText: Copyright 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

import 'platform_channels_demo_platform_interface.dart';

// Platform plugin keys channels
const channelMethods = "platform_channels_demo";

// Platform channel plugin methods
enum Methods {
  applicationName,
}

/// An implementation of [PlatformChannelsDemoPlatform] that uses method channels.
class MethodChannelPlatformChannelsDemo extends PlatformChannelsDemoPlatform {
  /// The method channel used to interact with the native platform.
  @visibleForTesting
  final methods = const MethodChannel(channelMethods);

  @override
  Future<String?> getApplicationName(String? prefix) async {
    if (prefix == null) {
      return await methods.invokeMethod<String>(Methods.applicationName.name);
    } else {
      return await methods.invokeMethod<String>(Methods.applicationName.name, {
        'prefix': prefix,
      });
    }
  }
}
