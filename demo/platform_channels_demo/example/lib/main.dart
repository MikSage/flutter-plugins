// SPDX-FileCopyrightText: Copyright 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import 'package:flutter/material.dart';
import 'package:internal_aurora/list_item_data.dart';
import 'package:internal_aurora/list_item_info.dart';
import 'package:internal_aurora/list_separated.dart';
import 'package:internal_aurora/theme/colors.dart';
import 'package:internal_aurora/theme/theme.dart';
import 'package:platform_channels_demo/platform_channels_demo.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final PlatformChannelsDemo _plugin = PlatformChannelsDemo();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: internalTheme,
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Platform Channels Demo'),
        ),
        body: ListSeparated(
          children: [
            const ListItemInfo("""
            An example of a platform-dependent plugin that works with the
            Aurora OS via the Platform Channels.
            """),
            ListItemData(
              'Application name',
              InternalColors.purple,
              description:
                  'Getting the application name through Platform Channels',
              future: _plugin.getApplicationName(),
            ),
            ListItemData(
              'Application name & Prefix',
              InternalColors.green,
              description:
                  'Getting the application name and passing data through Platform Channels',
              future: _plugin.getApplicationName(
                prefix: 'Name: ',
              ),
            ),
          ],
        ),
      ),
    );
  }
}
