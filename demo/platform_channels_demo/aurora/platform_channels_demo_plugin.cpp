/**
 * SPDX-FileCopyrightText: Copyright 2024 Open Mobile Platform LLC
 * <community@omp.ru> SPDX-License-Identifier: BSD-3-Clause
 */
#include <platform_channels_demo/platform_channels_demo_plugin.h>

namespace Channels {
constexpr auto Methods = "platform_channels_demo";
} // namespace Channels

namespace Methods {
constexpr auto ApplicationName = "applicationName";
} // namespace Methods

namespace Encodable {
template <typename T> inline bool TypeIs(const EncodableValue val) {
  return std::holds_alternative<T>(val);
}

template <typename T> inline const T GetValue(EncodableValue val) {
  return std::get<T>(val);
}

inline std::string GetString(const EncodableMap &map, const std::string &key) {
  auto it = map.find(EncodableValue(key));
  if (it != map.end() && TypeIs<std::string>(it->second))
    return GetValue<std::string>(it->second);
  return std::string();
}
} // namespace Encodable

void PlatformChannelsDemoPlugin::RegisterWithRegistrar(
    flutter::PluginRegistrar *registrar) {
  // Create MethodChannel with StandardMethodCodec
  auto methodChannel = std::make_unique<MethodChannel>(
      registrar->messenger(), Channels::Methods,
      &flutter::StandardMethodCodec::GetInstance());

  // Create plugin
  std::unique_ptr<PlatformChannelsDemoPlugin> plugin(
      new PlatformChannelsDemoPlugin(std::move(methodChannel)));

  // Register plugin
  registrar->AddPlugin(std::move(plugin));
}

PlatformChannelsDemoPlugin::PlatformChannelsDemoPlugin(
    std::unique_ptr<MethodChannel> methodChannel)
    : m_methodChannel(std::move(methodChannel)) {
  // Create MethodHandler
  RegisterMethodHandler();
}

void PlatformChannelsDemoPlugin::RegisterMethodHandler() {
  m_methodChannel->SetMethodCallHandler(
      [this](const MethodCall &call, std::unique_ptr<MethodResult> result) {
        if (call.method_name().compare(Methods::ApplicationName) == 0) {
          result->Success(onGetApplicationName(call));
        } else {
          result->Success();
        }
      });
}

EncodableValue
PlatformChannelsDemoPlugin::onGetApplicationName(const MethodCall &call) {
  if (Encodable::TypeIs<EncodableMap>(*call.arguments())) {
    // Get arguments
    const EncodableMap params =
        Encodable::GetValue<EncodableMap>(*call.arguments());
    // Get prefix
    std::string prefix = Encodable::GetString(params, "prefix");
    // Return data with prefix
    return prefix + aurora::GetApplicationName();
  }
  return aurora::GetApplicationName();
}
