// SPDX-FileCopyrightText: Copyright 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
library dbus_demo;

import 'package:dbus/dbus.dart';
import 'package:dbus_demo/ru_omp_deviceinfo_Features.dart';

class DbusDemo {
  /// Implementation of a method for obtaining a device model.
  Future<String?> getDeviceName() async {
    // Initialization of the D-Bus client
    final client = DBusClient.system();

    // Initializing an object
    final features = RuOmpDeviceinfoFeatures(
      client,
      'ru.omp.deviceinfo',
      DBusObjectPath('/ru/omp/deviceinfo/Features'),
    );

    // Method Execution
    final deviceName = await features.callgetDeviceModel();

    // Closing the D-Bus client
    await client.close();

    // Returning the result
    return deviceName == '' ? null : deviceName;
  }
}
