## Updated: 04/24/2024 13:28:56 PM

## Info

- Last tag: connectivity_plus_aurora-0.5.0
- Released: 2

## Versions

- Version: connectivity_plus_aurora-0.5.0 (22/04/2024)
- Version: connectivity_plus_aurora-0.0.1 (01/02/2024)

### Version: connectivity_plus_aurora-0.5.0 (22/04/2024)

#### Change

- Use flutter interface client_wrapper.
[sensors_plus_aurora][change] Use flutter interface client_wrapper.
[package_info_plus_aurora][change] Use flutter interface client_wrapper.
[flutter_keyboard_visibility_aurora][change] Use flutter interface client_wrapper.
[camera_aurora][change] Use flutter interface client_wrapper.
[sqflite_aurora][change] Use flutter interface client_wrapper.
[flutter_secure_storage_aurora][feature] Add example.
[flutter_local_notifications_aurora][feature] Add example.
[cached_network_image][feature] Add example for cached_network_image.
[flutter_cache_manager][feature] Add example for flutter_cache_manager.
[sqflite_aurora][feature] Add example for sqflite_aurora.
[camera_aurora][feature] Add example for camera_aurora.
[device_info_plus_aurora][feature] Add example for device_info_plus_aurora, fix AuroraOS 5.
[xdga_directories][bug] Check for duplicates in xdga_directories.
[change] Switch to a more recent changeln.

### Version: connectivity_plus_aurora-0.0.1 (01/02/2024)

#### Feature

- Add example application.
- The Aurora OS implementation of connectivity_plus.
