# Secure storage aurora example 

Demonstrates how to use the flutter_secure_storage_aurora plugin.

## Usage

Get dependency

```shell
flutter-aurora pub get
```

Build aurora application

```shell
flutter-aurora build aurora --release
```
