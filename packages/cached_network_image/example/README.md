# Cached network image

Demonstrates how to use the `cached_network_image` plugin.

## Usage

Get dependency

```shell
flutter-aurora pub get
```

Build aurora application

```shell
flutter-aurora build aurora --release
```
