## Updated: 04/24/2024 13:28:57 PM

## Info

- Last tag: flutter_cache_manager-0.5.0
- Released: 1

## Versions

- Version: flutter_cache_manager-0.5.0 (22/04/2024)

### Version: flutter_cache_manager-0.5.0 (22/04/2024)

#### Feature

- Add example for flutter_cache_manager.
[sqflite_aurora][feature] Add example for sqflite_aurora.
[camera_aurora][feature] Add example for camera_aurora.
[device_info_plus_aurora][feature] Add example for device_info_plus_aurora, fix AuroraOS 5.
[xdga_directories][bug] Check for duplicates in xdga_directories.
[change] Switch to a more recent changeln.
