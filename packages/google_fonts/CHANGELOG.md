## Updated: 04/24/2024 13:29:00 PM

## Info

- Last tag: google_fonts-0.5.0
- Released: 1

## Versions

- Version: google_fonts-0.5.0 (22/04/2024)

### Version: google_fonts-0.5.0 (22/04/2024)

#### Feature

- Add example application.
