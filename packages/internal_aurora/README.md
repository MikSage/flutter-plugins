# internal_aurora

A package to simplify and organize plugins example.

## Features

- `ListSeparated` - the basic component for building the page.
- `ListItemInfo` - a block with information about the plugin that every example should have.
- `ListItemData` - a list element that can process feature & stream, value values.
- `ListButton` - button stylized as examples.
- `AppBarAction` - stylized AppBar.
- `ListItemFormSuccess` - a stylized block for forms displaying success.
- `InternalColors` - Standard colors for examples, with an expanded set.

**pubspec.yaml**

```yaml
dependencies:
  internal_aurora:
    git:
      url: https://gitlab.com/omprussia/flutter/flutter-plugins.git
      ref: internal_aurora-0.5.0
      path: packages/internal_aurora
```

***.dart**

```dart
MaterialApp(
  // Common theme for examples
  theme: internalTheme,
  home: Scaffold(
    appBar: AppBar(
      // Title
      title: const Text('Internal'),
      // Add button for refresh data.
      actions: [AppBarAction(onPressed: _init)],
    ),
    // Custom list for widgets with separated.
    body: ListSeparated(
      children: [
        // The plugin must have a description in the example.
        const ListItemInfo("""
        This is an example implementation of the example for the plugin.
        """),

        // We have a data block in which we can display
        // information in the formats: stream, future, value.
        ListItemData(
          'Check Aurora OS',
          InternalColors.purple,
          description: 'Displays whether the current system is Aurora OS',
          widthData: 140,
          stream: _impl.onIsAurora(),
          builder: (value) => value?.toString().toUpperCase(),
        ),

        // Custom widget in ListItemData
        ListItemData('Check Aurora OS', InternalColors.orange,
            description: """
            Displays whether the current system is Aurora OS
            with custom widget
            """,
            widthData: 140,
            stream: _impl.onIsAurora(), builder: (value) {
          final boxDecoration = BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(30.0),
          );
          switch (value) {
            case true:
              return Container(
                decoration: boxDecoration,
                padding: const EdgeInsets.all(8),
                child: const Icon(
                  Icons.verified,
                  color: Colors.green,
                ),
              );
            case false:
              return Container(
                decoration: boxDecoration,
                padding: const EdgeInsets.all(8),
                child: const Icon(
                  Icons.new_releases,
                  color: Colors.red,
                ),
              );
            default:
              return Container(
                decoration: boxDecoration,
                padding: const EdgeInsets.all(8),
                child: const Icon(
                  Icons.hourglass_bottom,
                  color: Colors.blueGrey,
                ),
              );
          }
        }),

        // We will demonstrate how the refresh feature works
        ListItemData(
          'Random number',
          InternalColors.coal,
          description: 'This random number should be updated after refresh',
          // If you do not specify the size, the data block will be displayed as a list
          widthData: null,
          future: _randomNumber,
          builder: (value) => value?.toString().toUpperCase(),
        ),

        // Button in common style for example
        ListButton('Refresh', InternalColors.green, onPressed: _init)
      ],
    ),
  ),
);
```
