## Updated: 04/24/2024 13:29:04 PM

## Info

- Last tag: xdga_directories-0.5.0
- Released: 2

## Versions

- Version: xdga_directories-0.5.0 (22/04/2024)
- Version: xdga_directories-0.0.1 (28/12/2023)

### Version: xdga_directories-0.5.0 (22/04/2024)

#### Bug

- Check for duplicates in xdga_directories.
[change] Switch to a more recent changeln.

#### Feature

- Add example application.

### Version: xdga_directories-0.0.1 (28/12/2023)

#### Feature

- Add changeln, update dependency ref, readme.
